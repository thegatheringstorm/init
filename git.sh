#!/bin/bash 

git config --global color.ui auto

git config --global alias.st status
git config --global alias.ci commit
git config --global alias.co checkout
git config --global alias.br 'branch -a' --replace-all